// <auto-generated />
namespace PhotoAlbum3.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class AddPostAbstract : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddPostAbstract));
        
        string IMigrationMetadata.Id
        {
            get { return "201211261036221_AddPostAbstract"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
