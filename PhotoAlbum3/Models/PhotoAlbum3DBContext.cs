﻿using System.Configuration;
using System.Data.Entity;
using System.Data.EntityClient;


namespace PhotoAlbum3.Models
{
    public class PhotoAlbum3DBContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 

        //System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<PhotoAlbum3.Models.PhotoAlbum3DBContext>());

        public PhotoAlbum3DBContext( )
            : base( "name=PhotoAlbum3DBContext" )
        {
        }

        public DbSet<Location> Locations { get; set; }

        public DbSet<ImageTags> ImageTags { get; set; }

        public DbSet<PhotoImage> PhotoImages { get; set; }

        
        

        protected override void OnModelCreating( DbModelBuilder modelBuilder )
        {
            modelBuilder.Entity<PhotoImage>( ).HasMany( t => t.TagsList )
                .WithMany( p => p.Images ).
                Map( m => { m.ToTable( "AssignedImageTags" ); 
                            m.MapLeftKey( "ImageId" ); 
                    m.MapRightKey( "TagId" ); } );

            //modelBuilder.Entity<PhotoImage>( ).HasMany( p => p.PeopleTaged ).WithMany( x => x.Images ).Map( m => { m.ToTable( "ImagePeopleTags" ); m.MapLeftKey( "ImageId" ); m.MapRightKey( "UserId" ); } );

           // modelBuilder.Entity<Comment>( ).HasRequired( p => p.Image ).WithMany( ).HasForeignKey( p => p.ImageId );

            modelBuilder.Entity<Comment>( ).HasRequired( u => u.User ).WithMany( ).HasForeignKey( u => u.UserId ).WillCascadeOnDelete( false );

            
        }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public static string GetConnectionString( )
        {
            string baseConnectionString = ConfigurationManager.ConnectionStrings["PhotoAlbum3DBContext"].ConnectionString;

            EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder( );
            entityBuilder.Provider = "System.Data.SqlClient";
            entityBuilder.ProviderConnectionString = "Server=8f21e05d-9aac-4cfa-9d89-a1160141efc0.sqlserver.sequelizer.com;Database=db8f21e05d9aac4cfa9d89a1160141efc0;User ID=rrqjheueiwpbswzv;Password=NDbihUopYxkGYxqnLXxZBkwaCv4U5tt6HHRgcAX3BwE27ou5Kxy3BdxBgaNTZAtE;";//"PhotoAlbum3DBContextServer";
            entityBuilder.Metadata = @"res://*/Model1.csdl|res://*/Model1.ssdl|res://*/Model1.msl";

            return entityBuilder.ToString( );
        }

       
    }
}
