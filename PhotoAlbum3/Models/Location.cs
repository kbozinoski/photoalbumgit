﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhotoAlbum3.Models
{
    public class Location
    {
        public virtual int ID { get; set; }
        public virtual string coordinates { get; set; }
        public virtual string description { get; set; }
    }
}
