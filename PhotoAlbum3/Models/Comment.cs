﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace PhotoAlbum3.Models
{
    public class Comment
    {
        public virtual int ID { get; set; }
        
        public virtual string Text { get; set; }

        [ForeignKey("ImageId")]
        public virtual PhotoImage Image { get; set; }
        public virtual int ImageId { get; set; }

        [ForeignKey( "UserId" )]
        public virtual UserProfile User { get; set; }
        public virtual int UserId { get; set; }
    }
}
