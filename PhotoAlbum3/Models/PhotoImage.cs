﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PhotoAlbum3.Models
{
    public class PhotoImage
    {
        public PhotoImage( )
        {
            //this.PeopleTaged = new List<UserProfile>( );
            //this.Comments = new List<Comment>( );
            //this.TagsList = new List<ImageTags>( );
        }

        [Key]
        public virtual int ID { get; set; }
        
        public virtual string LargeURL { get; set; }
        public virtual string SmalURL { get; set; }
        public virtual int Width { get; set; }
        public virtual int Height { get; set; }

        [ForeignKey("OwnerId")]
        public virtual UserProfile Owner { get; set; }
        public virtual int OwnerId { get; set; }
        
        [DisplayName("Location")]
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        public virtual int LocationId { get; set; }

        [DisplayName( "Photo Privacy" )]
        public virtual int privacy { get; set; }

        
        //public virtual ICollection<UserProfile> PeopleTaged { get; set; }
        
        //[InverseProperty("Comments")]
        //public virtual List<Comment> Comments { get; set; }


        public virtual ICollection<ImageTags> TagsList { get; set; }
    }
}