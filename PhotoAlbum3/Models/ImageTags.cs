﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PhotoAlbum3.Models
{
    public class ImageTags
    {
        public ImageTags( )
        {
            //Images = new List<PhotoImage>( );
        }
        public ImageTags( string ID )
        {
            this.ID = int.Parse( ID );
        }
        [Key]
        public virtual int ID { get; set; }
        [DisplayName("Tag Name")]
        public virtual string TagName { get; set; }

        public ICollection<PhotoImage> Images { get; set; }
    }
}
