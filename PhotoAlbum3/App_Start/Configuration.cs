﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using PhotoAlbum3.Models;

namespace PhotoAlbum3.App_Start
{
    public class Configuration : DbMigrationsConfiguration<PhotoAlbum3DBContext>
    {
        public Configuration ()
	{
        AutomaticMigrationsEnabled = true;
        AutomaticMigrationDataLossAllowed = true;
	}
        
    }
}