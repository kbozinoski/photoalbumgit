﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using PhotoAlbum3.Models;

namespace PhotoAlbum3.App_Start
{
    public class Configuration2 : DbMigrationsConfiguration<PhotoAlbum3DBContext>
    {
        public Configuration2 ()
	{
        AutomaticMigrationsEnabled = true;
        AutomaticMigrationDataLossAllowed = true;
	}
        
    }
}