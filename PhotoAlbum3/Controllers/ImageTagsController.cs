﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PhotoAlbum3.Models;

namespace PhotoAlbum3.Controllers
{
    public class ImageTagsController : Controller
    {
        private PhotoAlbum3DBContext db = new PhotoAlbum3DBContext();

        //
        // GET: /ImageTags/

        public ActionResult Index()
        {
            return View(db.ImageTags.ToList());
        }

        //
        // GET: /ImageTags/Details/5

        public ActionResult Details(int id = 0)
        {
            ImageTags imagetags = db.ImageTags.Find(id);
            if (imagetags == null)
            {
                return HttpNotFound();
            }
            return View(imagetags);
        }

        //
        // GET: /ImageTags/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ImageTags/Create

        [HttpPost]
        public ActionResult Create(ImageTags imagetags)
        {
            if (ModelState.IsValid)
            {
                db.ImageTags.Add(imagetags);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(imagetags);
        }

        //
        // GET: /ImageTags/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ImageTags imagetags = db.ImageTags.Find(id);
            if (imagetags == null)
            {
                return HttpNotFound();
            }
            return View(imagetags);
        }

        //
        // POST: /ImageTags/Edit/5

        [HttpPost]
        public ActionResult Edit(ImageTags imagetags)
        {
            if (ModelState.IsValid)
            {
                db.Entry(imagetags).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(imagetags);
        }

        //
        // GET: /ImageTags/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ImageTags imagetags = db.ImageTags.Find(id);
            if (imagetags == null)
            {
                return HttpNotFound();
            }
            return View(imagetags);
        }

        //
        // POST: /ImageTags/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ImageTags imagetags = db.ImageTags.Find(id);
            db.ImageTags.Remove(imagetags);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}