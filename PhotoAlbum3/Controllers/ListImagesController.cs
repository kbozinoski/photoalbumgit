﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PhotoAlbum3.Models;
using PhotoAlbum3.Utils;

namespace PhotoAlbum3.Controllers
{

    public class ListImagesController : Controller
    {
        private PhotoAlbum3DBContext db = new PhotoAlbum3DBContext( );


        //
        // GET: /ListImages/

        public ActionResult Index( )
        {
            string user_name = getActiveUserName( );
            ViewBag.AvailAbleTags = new SelectList( db.ImageTags, "ID", "TagName" );
            ViewBag.AvailAbleLocations = new SelectList( db.Locations, "ID", "Description" );
            return View( db.PhotoImages.Include( p => p.Location ).Where( p => ( p.privacy == 1 && p.Owner.UserName == user_name ) || p.privacy == 2 ) );
        }


        [HttpPost]
        public ActionResult Search( )
        {
            string[] tagArray = Request.Form["items"].Split( ',' );
            string user_name = getActiveUserName( );
            List<int> ids = new List<int>( );
            foreach ( string s in tagArray )
            {
                ids.Add( int.Parse( s ) );
            }
            IQueryable<PhotoImage> photos = db.PhotoImages.Where( p => p.TagsList.Any( x => ids.Contains( x.ID ) ) && ( ( p.privacy == 1 && p.Owner.UserName == user_name ) || p.privacy == 2 ) );
            ViewBag.AvailAbleTags = new SelectList( db.ImageTags, "ID", "TagName" );

            return View( "Index", photos );
        }

        //
        // GET: /ListImages/Details/5

        public ActionResult Details( int id = 0 )
        {
            //PhotoImage photoimage = db.PhotoImages.Find( id );

            string user_name = getActiveUserName( );
            PhotoImage photoimage;
            try
            {
                photoimage = db.PhotoImages.Single( p => ( ( p.privacy == 1 && p.Owner.UserName == user_name ) || p.privacy == 2 ) && p.ID == id );
            }
            catch ( Exception )
            {
                ViewBag.Error = "Photo is private or deleted!";
                return View( "Errors" );
            }

            Comment[] imgComment = ( from c in db.Comments where c.ImageId == id select c ).ToArray( );

            ViewBag.Comments = ( imgComment == null ) ? new Comment[0] : imgComment;
            if ( photoimage == null )
            {
                ViewBag.Error = "Photo is private or deleted!";
                return View( "Errors" );
            }
            return View( photoimage );
        }

        //
        // GET: /ListImages/Create
        [Authorize]
        public ActionResult Create( )
        {
            ViewBag.LocationId = new SelectList( db.Locations, "ID", "coordinates" );
            ViewBag.AvailAbleTags = new SelectList( db.ImageTags, "ID", "TagName" );
            return View( );
        }

        //
        // POST: /ListImages/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create( PhotoImage photoimage )
        {
            if ( ModelState.IsValid )
            {

                string user_name = getActiveUserName( );


                photoimage.OwnerId = GetActiveUserId( );

                string[] tmp = Request.Form["items"].Split( ',' );

                //photoimage.TagsList = db.ImageTags.Where( x => tmp.Contains( x.ID.ToString( ) ) ) as ICollection<ImageTags>;

                photoimage.TagsList = new List<ImageTags>( );
                foreach ( string s in tmp )
                {
                    int tmpId = int.Parse( s );
                    photoimage.TagsList.Add( db.ImageTags.Single( x => x.ID == tmpId ) );
                }
                SaveImageUtil.SaveImage( photoimage, Request, Server, "fileUpload", "Images" );
                db.PhotoImages.Add( photoimage );

                db.SaveChanges( );
                return RedirectToAction( "Index" );
            }

            ViewBag.LocationId = new SelectList( db.Locations, "ID", "coordinates", photoimage.LocationId );
            return View( photoimage );
        }



        [Authorize]
        [HttpPost]
        public ActionResult PublishComment( string pic_id, string comment )
        {
            if ( ModelState.IsValid )
            {

                Comment c = new Comment( );
                c.ImageId = int.Parse( pic_id );
                c.Text = comment;

                c.UserId = GetActiveUserId( );

                db.Comments.Add( c );
                db.SaveChanges( );

            }

            int tmpPicId = int.Parse( pic_id );
            Comment[] imgComment = ( from c in db.Comments where c.ImageId == tmpPicId select c ).Include( u => u.User ).ToArray( );

            ViewBag.Comments = ( imgComment == null ) ? new Comment[0] : imgComment;

            PhotoImage imgTMP = db.PhotoImages.Find( int.Parse( pic_id ) );
            return View( "Details", imgTMP );
        }


        //
        // GET: /ListImages/Edit/5
        [Authorize]
        public ActionResult Edit( int id = 0 )
        {
            PhotoImage photoimage;
            try
            {
                string user_name = getActiveUserName( );
                photoimage = CheckActiveUserOwnership( id );

                ViewBag.LocationId = new SelectList( db.Locations, "ID", "coordinates", photoimage.LocationId );
                MultiSelectList lista = new MultiSelectList( db.ImageTags, "ID", "TagName", photoimage.TagsList.Select( x => x.ID.ToString() ) );
                ViewBag.ListImageTags = lista;
            }
            catch ( Exception )
            {
                ViewBag.Error = "You don't have privilegies to edit!";
                return View( "Errors" );
            }
            if ( photoimage == null )
            {
                ViewBag.Error = "Picture deleted or insuficient privilegies";
                return View( "Errors" );
            }


            return View( photoimage );
        }

        //
        // POST: /ListImages/Edit/5
        [Authorize]
        [HttpPost]
        public ActionResult Edit( PhotoImage photoimage )
        {
            if ( ModelState.IsValid )
            {
                PhotoImage tmpImage = db.PhotoImages.Find( photoimage.ID );

                //List<int> tmp = Request.Form["items"].Split( ',' ).Select(x=>int.Parse(x)).ToList<int>();
                //IQueryable<ImageTags> tmpTags = db.ImageTags.Where( x => tmp.Contains( x.ID ) );
                  
                    
                    //( db.ImageTags.Where( x => tmp.Contains( x.ID ) ) );
                tmpImage.privacy = photoimage.privacy;
                tmpImage.LocationId = photoimage.LocationId;

                db.Entry( tmpImage ).State = EntityState.Modified;
                db.SaveChanges( );
                return RedirectToAction( "Index" );
            }

            return View( photoimage );
        }

        //
        // GET: /ListImages/Delete/5
        [Authorize]
        public ActionResult Delete( int id = 0 )
        {
            PhotoImage photoimage = db.PhotoImages.Find( id );
            if ( photoimage == null )
            {
                ViewBag.Error = "You don't have privilegies to delete!";
                return View( "Errors" );
                //return HttpNotFound( );
            }
            return View( photoimage );
        }

        //
        // POST: /ListImages/Delete/5

        [HttpPost, ActionName( "Delete" )]
        public ActionResult DeleteConfirmed( int id )
        {
            string user_name = getActiveUserName( );
            try
            {
                PhotoImage photoimage = CheckActiveUserOwnership( id );
                db.PhotoImages.Remove( photoimage );
                db.SaveChanges( );
            }
            catch ( Exception )
            {
                //ViewBag.Error = "You don't have privilegies to edit!";
                ViewBag.Error = "Picture deleted or insuficient privilegies";
                return View( "Errors" );
            }
            return RedirectToAction( "Index" );
        }


        protected override void Dispose( bool disposing )
        {
            db.Dispose( );
            base.Dispose( disposing );
        }

        private string getActiveUserName( )
        {
            return HttpContext.Request.RequestContext.HttpContext.User.Identity.Name;
        }

        private PhotoImage CheckActiveUserOwnership( int id )
        {
            string user_name = getActiveUserName( );
            return db.PhotoImages.Single( p => p.ID == id && p.Owner.UserName == user_name );
        }

        private int GetActiveUserId( )
        {
            string user_name = getActiveUserName( );
            return db.UserProfiles.Single( x => x.UserName == user_name ).UserId;
        }
    }
}