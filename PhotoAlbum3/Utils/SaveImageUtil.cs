﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using PhotoAlbum3.Models;

namespace PhotoAlbum3.Utils
{
    public class SaveImageUtil
    {
        public static PhotoImage SaveImage( PhotoImage photoimage, HttpRequestBase request,HttpServerUtilityBase server,string fileName, string folder)
        {
            var postedFile = request.Files[fileName];
            string path = server.MapPath( string.Format( "~/{0}/", folder ) ) + Path.GetFileName( DateTime.Now.ToString( "yyyyMMddHHmmssffff" ) )+Path.GetExtension(postedFile.FileName);

            
            photoimage.LargeURL = string.Format( "/{0}/", folder )+Path.GetFileName( path );
            photoimage.SmalURL = string.Format( "/{0}/", folder )+Path.GetFullPath( path );
            
            postedFile.SaveAs( path );
            return photoimage;
        }
    }
}